
import requests
import os

api_url = os.environ['CI_API_V4_URL']
commit = os.environ['CI_COMMIT_BRANCH']
token = os.environ['SNK_CI_JOB_TOKEN']

r = requests.get(f"{api_url}/projects/51574023/repository/commits/{commit}/merge_requests?per_page=100", headers={'Authorization': f'Bearer {token}'})

print(r.status_code)
print(r.json())